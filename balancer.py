# Balance an investment portfolio through integer approximation of the
# solution to a linear polynomial.


class Security:
    def __init__(self, price, weight, held=0):
        self.price = price
        self.weight = weight
        self.held = held

def solve(total, entries):
    ''' Generate an investment recommendation for the provided securities 
        and total funds available.

        Total represents the total funds, including funds already invested.
        The solution also includes funds already invested.'''
    answers = {}

    while len(entries) > 0:
        estimates = {}
        errors = {}
        generateEstimates(entries, total, estimates, errors)

        best = findEstimateBelowHeld(estimates, entries)
        if best == None:
            best = findMinError(errors)

        buyQ = int(estimates[best])
        buyQ = max(buyQ, entries[best].held)
        answers[best] = buyQ

        total -= buyQ * entries[best].price
        del entries[best]
    return answers

def findEstimateBelowHeld(estimates, entries):
    for e in estimates:
        if estimates[e] < entries[e].held:
            return e
    return None

def generateEstimates(entries, total, estimates, errors):
    ''' Populates the estimates and errors variables with purchase quantity
        estimates and errors associated with those estimates 
        respectively.'''
    totalWeight = sum([entries[e].weight for e in entries])
    for e in entries:
        estimate = (
                total / entries[e].price * 
                entries[e].weight / totalWeight
        )
        estimates[e] = estimate
        errors[e] = abs(estimate - int(estimate))

def findMinError(errors):
    '''Finds and returns the index of the smallest entry in errors.'''
    minError = -1
    best = None
    for e in errors:
        if minError == -1 or errors[e] < minError:
            best = e
            minError = errors[e]
    return best
