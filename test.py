import unittest

from balancer import *

class IATest(unittest.TestCase):
    def testEmpty(self):
        self.assertEqual(solve(10, {}), {})

    def testOne(self):
        self.assertEqual(solve(10, {0: Security(1, 1)}), {0: 10})

    def testGoldExample(self):
        self.assertEqual(
            solve(500, {'silver': Security(40, 1),'gold':Security(200,1)}),
            {'silver': 6, 'gold': 1}
        )

    def testGoldExampleFiveSilverInvested(self):
        self.assertEqual(
            solve(500, 
                {'silver': Security(40, 1, 5), 'gold':Security(200,1)}),
            {'silver': 6, 'gold': 1}
        )

    def testGoldExampleSixSilverInvested(self):
        self.assertEqual(
            solve(500, 
                {'silver': Security(40, 1, 8), 'gold':Security(200,1)}),
            {'silver': 8, 'gold': 0}
        )

    def testGoldExampleSixSilverInvested(self):
        self.assertEqual(
            solve(500, 
                {'silver': Security(40, 1), 'gold':Security(200,1,2)}),
            {'silver': 2, 'gold': 2}
        )

    def testGoldRoundingExample(self):
        self.assertEqual(
                solve(500, 
                    {'silver': Security(39, 1),'gold':Security(200,1)}), 
                {'silver': 7, 'gold': 1}
        )

    def testZeroSecurity(self):
        self.assertEqual(
                solve(500, 
                    {'silver': Security(40, 0), 'gold':Security(200,1)}), 
                {'silver': 0, 'gold': 2}
        )

    def testDifferentSecuritys(self):
        self.assertEqual(
                solve(100, {0: Security(1, 1), 1: Security(1, 9)}), 
                {0: 10, 1: 90}
        )

if __name__ == '__main__':
    unittest.main()
